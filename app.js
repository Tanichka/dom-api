const url = 'https://rickandmortyapi.com/api/character';
fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        return rawData.map(character => {
            //all needed data is listed below as an entity 
            let created = character.created;
            let species = character.species;
            let img = character.image;
            let episodes = character.episode;
            let name = character.name;
            let location = character.location;



            let templateElement = document.getElementsByClassName("empty")[0];
            let newElement = templateElement.cloneNode(true);
            
            newElement.classList.remove("empty");
            let nameElement = newElement.getElementsByClassName("character-name")[0];
            nameElement.innerHTML = name;
            
            let locationElement = newElement.getElementsByClassName("character-localization")[0];
            let locationLinkElement = locationElement.getElementsByTagName("a")[0];
            locationLinkElement.href = location.url;
            locationLinkElement.innerHTML= location.name;
        
            
            let episodesListElement = newElement.getElementsByClassName("episodes-list")[0];
            episodes.forEach(episodeUrl => {
                let newItem = episodesListElement.getElementsByClassName("empty")[0].cloneNode(true);
                let newListItem = newItem.getElementsByTagName("li")[0];
                
                newItem.classList.remove("empty")
                newItem.href = episodeUrl;
                newListItem.innerHTML = episodeUrl;
                
                episodesListElement.appendChild(newItem);
            });
            
            let imgWrapperElement = newElement.getElementsByClassName("character-picture")[0];
            let imgElement = imgWrapperElement.getElementsByTagName("img")[0];
            imgElement.src = img;
            
            let createdElement = newElement.getElementsByClassName("character-date_creation")[0];
            createdElement.innerHTML = created;
            
            let speciesElement = newElement.getElementsByClassName("character-species")[0];
            speciesElement.innerHTML = species;
             
            let cardContainer = document.getElementsByClassName("card-wrapper")[0];
            cardContainer.appendChild(newElement);

        });
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });
